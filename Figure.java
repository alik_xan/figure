package figure;
public interface Figure {
    public abstract double getArea();
    public abstract String getName();
}