package figure;
public class Main {
    public static void main(String[] args) {
        Figure[] figure = {
            new Rectangle(8, 12),
            new Circle(10)
        };
        for (Figure fig: figure) System.out.println(fig.getName() + ": ������� = " + fig.getArea());
    }
}